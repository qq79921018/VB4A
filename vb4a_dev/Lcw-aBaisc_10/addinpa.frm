VERSION 5.00
Object = "{3B7C8863-D78F-101B-B9B5-04021C009402}#1.2#0"; "RICHTX32.OCX"
Begin VB.Form addinpa 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "插入"
   ClientHeight    =   5010
   ClientLeft      =   7275
   ClientTop       =   4320
   ClientWidth     =   9030
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5010
   ScaleWidth      =   9030
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton Command2 
      Caption         =   "Command2"
      Height          =   375
      Left            =   7560
      TabIndex        =   4
      Top             =   4560
      Width           =   1335
   End
   Begin VB.CommandButton Command1 
      Caption         =   "插入"
      Height          =   375
      Left            =   6120
      TabIndex        =   3
      Top             =   4560
      Width           =   1335
   End
   Begin VB.ComboBox Combo2 
      Height          =   300
      Left            =   3120
      TabIndex        =   2
      Top             =   4560
      Width           =   2895
   End
   Begin RichTextLib.RichTextBox RichTextBox1 
      Height          =   4335
      Left            =   120
      TabIndex        =   1
      Top             =   120
      Width           =   8775
      _ExtentX        =   15478
      _ExtentY        =   7646
      _Version        =   393217
      ScrollBars      =   2
      AutoVerbMenu    =   -1  'True
      TextRTF         =   $"addinpa.frx":0000
   End
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   120
      TabIndex        =   0
      Top             =   4560
      Width           =   2895
   End
End
Attribute VB_Name = "addinpa"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public inspos As Long

Private Sub Combo1_Click()
    On Error GoTo ErrHand

    Debug.Print Combo1.ListIndex
    Combo2.Clear
    Select Case Combo1.ListIndex
        
    Case 0 'if
        Combo2.AddItem ("If Then")
        Combo2.AddItem ("If Then Else")
        Combo2.AddItem ("If Then ElseIf Then")
        Combo2.AddItem ("If Then ElseIf Then Else")
    Case 1 'do
        'Combo2.AddItem("")
        Combo2.AddItem ("Do While")
        Combo2.AddItem ("Do Until")
        Combo2.AddItem ("While")
    Case 2 'for
        Combo2.AddItem ("For Each")
        Combo2.AddItem ("For To")
    Case 3 'on error
        Combo2.AddItem ("On Error")
    Case 4 ' select case
        Combo2.AddItem ("Select Case")
    Case Else
        
    End Select

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Combo1_Click", vbExclamation
End Sub

Private Sub Command1_Click()
    On Error GoTo ErrHand

    If Combo1.text = "" Or Combo2.text = "" Then
        If Form1.CH Then
            MsgBox "请先选择要添加的语句！", vbCritical + vbOKOnly, ""
        Else
            MsgBox "Please select the sentence to be added first!", vbCritical + vbOKOnly, ""
        End If
    End If
    
    Select Case Combo1.ListIndex
    Case 0
        
        Select Case Combo2.ListIndex
        Case 0
            RichTextBox1.text = "If <Expression> Then <Statement>"
        Case 1
            RichTextBox1.text = "If <Expression> Then" & vbCrLf & _
            "<Statement1>" & vbCrLf & "Else" & vbCrLf & "<Statement2>" & vbCrLf & _
            "End if"
            
        Case 2
            RichTextBox1.text = "If <Expression1> Then" & vbCrLf & _
            "<Statement1>" & vbCrLf & "ElseIf <Expression2> Then" & vbCrLf & "<Statement2>" & _
            vbCrLf & "End if"
        Case 3
            RichTextBox1.text = "If <Expression1> Then" & vbCrLf & _
            "<Statement1>" & vbCrLf & "ElseIf <Expression2> Then" & vbCrLf & "<Statement2>" & _
            vbCrLf & "Else " & vbCrLf & "<Statement3>" & vbCrLf & "End if"
        End Select
        
    Case 1
        
        Select Case Combo2.ListIndex
        Case 0
            RichTextBox1.text = "Do" & vbCrLf & "<Statement>" & vbCrLf & "While <Expression>"
        Case 1
            RichTextBox1.text = "Do" & vbCrLf & "<Statement>" & vbCrLf & "Until <Expression>"
        Case 2
            RichTextBox1.text = "While <Expression>" & vbCrLf & "<Statement>" & vbCrLf & "End While"
        Case Else
            
        End Select
        
    Case 2
        
        Select Case Combo2.ListIndex
        Case 0
            RichTextBox1.text = "For Each <Identifier> In <Expression>" _
            & vbCrLf & "'<Statement>" & vbCrLf & "Next [<Identifier>]"
        Case 1
            RichTextBox1.text = "For <Identifier> = <Expression1> To <Expression2> [Step Expression3]" _
            & vbCrLf & "'<Statement>" & vbCrLf & "Next [<Identifier>]"
        Case Else
            
        End Select
        
    Case 3
        
        Select Case Combo2.ListIndex
        Case 0
        If Form1.CH = False Then
            RichTextBox1.text = "On Error" & vbCrLf _
            & "Case AssertionFailure" & vbCrLf _
            & "'    <Statement1> " & vbCrLf _
            & "Case ConversionError" & vbCrLf _
            & "'    <Statement2> " & vbCrLf _
            & "Case FileAlreadyExistsError" & vbCrLf _
            & "'    <Statement3> " & vbCrLf _
            & "Case FileIOError" & vbCrLf _
            & "'    <Statement4> " & vbCrLf _
            & "Case IllegalArgumentError" & vbCrLf _
            & "'    <Statement5> " & vbCrLf _
            & "Case IndexOutOfBoundsError" & vbCrLf _
            & "'    <Statement6> " & vbCrLf _
            & "Case NoSuchFileError" & vbCrLf _
            & "'    <Statement7> " & vbCrLf _
            & "Case PropertyAccessError" & vbCrLf _
            & "'    <Statement8> " & vbCrLf _
            & "Case UninitializedInstanceError" & vbCrLf _
            & "'    <Statement9> " & vbCrLf _
            & "Case UnknownFileHandleError" & vbCrLf _
            & "'    <Statement10> " & vbCrLf _
            & "Case UnknownIdentifierError" & vbCrLf _
            & "'    <Statement11> " & vbCrLf & "Case Else" & vbCrLf & "'    <Statement12> " & vbCrLf _
            & "End Error"
        Else
            RichTextBox1.text = "On Error" & vbCrLf _
            & "Case AssertionFailure" & vbCrLf _
            & "'    <Statement1> 断言失败" & vbCrLf _
            & "Case ConversionError" & vbCrLf _
            & "'    <Statement2> 转换错误" & vbCrLf _
            & "Case FileAlreadyExistsError" & vbCrLf _
            & "'    <Statement3> 文件已存在" & vbCrLf _
            & "Case FileIOError" & vbCrLf _
            & "'    <Statement4> 文件IO错误" & vbCrLf _
            & "Case IllegalArgumentError" & vbCrLf _
            & "'    <Statement5> 非法参数" & vbCrLf _
            & "Case IndexOutOfBoundsError" & vbCrLf _
            & "'    <Statement6> 索引超出范围" & vbCrLf _
            & "Case NoSuchFileError" & vbCrLf _
            & "'    <Statement7> 文件不存在" & vbCrLf _
            & "Case PropertyAccessError" & vbCrLf _
            & "'    <Statement8> 属性访问错误" & vbCrLf _
            & "Case UninitializedInstanceError" & vbCrLf _
            & "'    <Statement9> 未初始化示例" & vbCrLf _
            & "Case UnknownFileHandleError" & vbCrLf _
            & "'    <Statement10> 未知文件号" & vbCrLf _
            & "Case UnknownIdentifierError" & vbCrLf _
            & "'    <Statement11> 未知的标识符" & vbCrLf & "Case Else" & vbCrLf & "'    <Statement12> 其他任何类型的错误" & vbCrLf _
            & "End Error"
        End If
            
        Case Else
            
        End Select
        
    Case 4
        
        Select Case Combo2.ListIndex
        Case 0
            RichTextBox1.text = "Select <Expression>" & vbCrLf _
            & "Case <CaseExpression1>" & vbCrLf _
            & "'    <Statement1> " & vbCrLf _
            & "Case <CaseExpression2, CaseExpression3>" & vbCrLf _
            & "'    <Statement2> " & vbCrLf _
            & "Case Else" & vbCrLf _
            & "'    <Statement31> " & vbCrLf _
            & "End Select"
        Case Else
            
        End Select
        
    Case Else
    End Select
    
    
    
    If Form1.CH Then
        RichTextBox1.text = Replace(RichTextBox1.text, "Expression", "表达式")
        RichTextBox1.text = Replace(RichTextBox1.text, "Statement", "语句")
        RichTextBox1.text = Replace(RichTextBox1.text, "Identifier ", "标识 ")
    Else
        
    End If
    'KeyColor Me, RichTextBox1, vbBlue, B4Akeywords
    'StringColor Me, RichTextBox1, vbRed

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub


    


Private Sub Command2_Click()
    Form3.Str2add = Me.RichTextBox1.text
    Call Form3.refreshText2(inspos)
    Unload Me

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command2_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    
    If Form1.CH Then
        Command1.Caption = "编辑"
        Command2.Caption = "插入"
        Combo1.Clear
        Combo2.Clear
        
        Combo1.AddItem ("If结构")
        Combo1.AddItem ("Do/While结构")
        Combo1.AddItem ("For结构")
        Combo1.AddItem ("On Error结构")
        Combo1.AddItem ("Select Case结构")
        
    Else
        Command2.Caption = "Insert"
        Command1.Caption = "Edit"
        Combo1.AddItem ("If")
        Combo1.AddItem ("Do/While")
        Combo1.AddItem ("For")
        Combo1.AddItem ("On Error")
        Combo1.AddItem ("Select Case")
        
    End If
    
    RichTextBox1.text = ""
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub
