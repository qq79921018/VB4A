VERSION 5.00
Begin VB.Form Form6 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "定义变量"
   ClientHeight    =   2190
   ClientLeft      =   7500
   ClientTop       =   6315
   ClientWidth     =   3735
   LinkTopic       =   "Form6"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   2190
   ScaleWidth      =   3735
   Begin VB.CheckBox Check2 
      Caption         =   "Check2"
      Enabled         =   0   'False
      Height          =   375
      Left            =   2280
      TabIndex        =   8
      Top             =   120
      Width           =   1335
   End
   Begin VB.ComboBox Combo2 
      Height          =   300
      Left            =   120
      TabIndex        =   7
      Text            =   "Combo2"
      Top             =   120
      Width           =   1935
   End
   Begin VB.TextBox Text2 
      Alignment       =   2  'Center
      Height          =   270
      Left            =   120
      TabIndex        =   6
      Text            =   "()"
      Top             =   1800
      Width           =   1935
   End
   Begin VB.CheckBox Check1 
      Caption         =   "define as array"
      Height          =   255
      Left            =   120
      TabIndex        =   5
      Top             =   1440
      Width           =   1695
   End
   Begin VB.CommandButton Command1 
      Caption         =   "添加"
      Default         =   -1  'True
      Height          =   1455
      Left            =   2280
      TabIndex        =   4
      Top             =   600
      Width           =   1335
   End
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   840
      TabIndex        =   3
      Text            =   "Integer"
      Top             =   960
      Width           =   1215
   End
   Begin VB.TextBox Text1 
      Height          =   270
      Left            =   840
      TabIndex        =   1
      Top             =   600
      Width           =   1215
   End
   Begin VB.Label Label2 
      AutoSize        =   -1  'True
      Caption         =   "类型："
      Height          =   180
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   540
   End
   Begin VB.Label Label1 
      AutoSize        =   -1  'True
      Caption         =   "变量名："
      Height          =   180
      Left            =   120
      TabIndex        =   0
      Top             =   600
      Width           =   720
   End
End
Attribute VB_Name = "Form6"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public DefineReady As Boolean
Public DefinePos As Long
Public VAddmode As Integer

Private Sub Check1_Click()
    On Error GoTo ErrHand

    If Check1.value = 1 Then
        Text2.Enabled = True
    Else
        Text2.Enabled = False
    End If
    Debug.Print "check1:"; Check1.value

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Check1_Click", vbExclamation
End Sub

Private Sub Combo2_Click()
    On Error GoTo ErrHand

    If Combo2.ListIndex = 0 Then
        Check1.Enabled = True
        If VAddmode = 0 Then Check2.Enabled = True
    Else
        Check1.value = 0
        Check1.Enabled = False
        If VAddmode = 0 Then Check2.Enabled = False
    End If

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Combo2_Click", vbExclamation
End Sub

Private Sub Command1_Click()
    On Error GoTo ErrHand

    Dim Arrstr As String
    Dim statStr As String
    Arrstr = ""
    statStr = ""
    If Check1.value = 1 Then
        If Mid(Text2.text, 1, 1) = "(" And Mid(Text2.text, Len(Text2.text), 1) = ")" Then Arrstr = Text2.text
    End If
    
    If Check2.value = 1 And Check2.Enabled = True Then
        statStr = "Static "
    End If
    
    Dim PrefixStr As String
    If Combo2.ListIndex = 0 Then PrefixStr = "Dim " Else PrefixStr = "Const "
    
    If VAddmode = 0 Then
        Form3.Text1.text = statStr & PrefixStr & Trim(Me.Text1.text) & " As " & Me.Combo1.text & Arrstr & vbCrLf & Form3.Text1.text
    Else
        Form3.Str2add = PrefixStr & Trim(Me.Text1.text) & " As " & Me.Combo1.text & Arrstr & vbCrLf
        Call Form3.refreshText2(DefinePos)
    End If
    'Call Form3.Command6_Click
    Unload Me

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    Me.Combo1.Clear
    Me.Combo1.AddItem ("Integer")
    Me.Combo1.AddItem ("Single")
    Me.Combo1.AddItem ("Double")
    Me.Combo1.AddItem ("Byte")
    Me.Combo1.AddItem ("String")
    Me.Combo1.AddItem ("Boolean")
    Me.Combo1.AddItem ("Variant")
    Me.Combo1.AddItem ("Component")
    
    Me.Combo2.Clear
    Me.Combo2.AddItem ("Variable")
    If VAddmode = 0 Then Me.Combo2.AddItem ("Const")
    Call Form1.lan_changE(Form1.CH)
    If Form1.CH Then
        Check1.Caption = "定义为数组"
    Else
        Check1.Caption = "Define as array"
    End If
    Text2.Enabled = False

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub
