VERSION 5.00
Begin VB.Form Form7 
   BorderStyle     =   4  'Fixed ToolWindow
   ClientHeight    =   3660
   ClientLeft      =   1230
   ClientTop       =   4980
   ClientWidth     =   5025
   LinkTopic       =   "Form7"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   3660
   ScaleWidth      =   5025
   Begin VB.ComboBox Combo2 
      Height          =   300
      Left            =   1800
      TabIndex        =   10
      Text            =   "Combo2"
      Top             =   2520
      Width           =   2175
   End
   Begin VB.CommandButton Command2 
      Caption         =   "确定"
      Default         =   -1  'True
      Height          =   495
      Left            =   1080
      TabIndex        =   4
      Top             =   3000
      Width           =   2895
   End
   Begin VB.Frame Frame1 
      Caption         =   "参数列表"
      Height          =   1935
      Left            =   120
      TabIndex        =   2
      Top             =   480
      Width           =   4815
      Begin VB.TextBox Text2 
         Height          =   270
         Left            =   1080
         TabIndex        =   7
         Top             =   360
         Width           =   1335
      End
      Begin VB.ComboBox Combo1 
         Height          =   300
         Left            =   1080
         TabIndex        =   6
         Top             =   840
         Width           =   1335
      End
      Begin VB.CommandButton Command1 
         Caption         =   "添加参数"
         Height          =   375
         Left            =   480
         TabIndex        =   5
         Top             =   1320
         Width           =   1455
      End
      Begin VB.ListBox List1 
         Height          =   1500
         ItemData        =   "Form7.frx":0000
         Left            =   2520
         List            =   "Form7.frx":0002
         TabIndex        =   3
         Top             =   240
         Width           =   2175
      End
      Begin VB.Label Label2 
         AutoSize        =   -1  'True
         Caption         =   "参数名："
         Height          =   180
         Left            =   120
         TabIndex        =   9
         Top             =   360
         Width           =   720
      End
      Begin VB.Label Label3 
         AutoSize        =   -1  'True
         Caption         =   "参数类型："
         Height          =   180
         Left            =   120
         TabIndex        =   8
         Top             =   840
         Width           =   900
      End
   End
   Begin VB.TextBox Text1 
      Height          =   270
      Left            =   1800
      TabIndex        =   1
      Top             =   120
      Width           =   2175
   End
   Begin VB.Label Label4 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "type:"
      Height          =   180
      Left            =   0
      TabIndex        =   11
      Top             =   2520
      Width           =   1530
   End
   Begin VB.Label Label1 
      Alignment       =   1  'Right Justify
      AutoSize        =   -1  'True
      Caption         =   "name:"
      Height          =   180
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   1410
   End
End
Attribute VB_Name = "Form7"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public addtype As String
Dim csstr(1 To 50) As String
Dim cscounter As Integer
Dim strbefore As String

Private Sub Command1_Click()
    On Error GoTo ErrHand

    Me.List1.AddItem (Me.Text2.text & " as " & Me.Combo1.text)
    cscounter = cscounter + 1
    csstr(cscounter) = Me.Text2.text & " As " & Me.Combo1.text
    
    Text2.text = ""
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Command2_Click()
    On Error GoTo ErrHand

    
    If Text1.text = "" Then
        
        Unload Me
        Exit Sub
    End If
    
    strbefore = Form3.Text1.text
    
    Select Case addtype
        
    Case "Sub"
        
        Form3.Text1.text = addtype & " " & Text1.text & "("
        If cscounter < 2 Then
            Form3.Text1.text = Form3.Text1.text & csstr(1) & ")"
        Else
            For i = 1 To cscounter
                Form3.Text1.text = Form3.Text1.text & csstr(i) & ", "
            Next i
            Form3.Text1.text = Mid(Form3.Text1.text, 1, Len(Form3.Text1.text) - 2) & ")"
        End If
        Form3.Text1.text = Form3.Text1.text & vbCrLf & vbCrLf & vbCrLf
        
        Form3.Text1.text = Form3.Text1.text & "End " & addtype & vbCrLf & vbCrLf
        
        Form3.Text1.text = Form3.Text1.text & strbefore & vbCrLf & vbCrLf
        
    Case "Function"
        
        Form3.Text1.text = addtype & " " & Text1.text & "("
        If cscounter < 2 Then
            Form3.Text1.text = Form3.Text1.text & csstr(1) & ")"
        Else
            For i = 1 To cscounter
                Form3.Text1.text = Form3.Text1.text & csstr(i) & ", "
            Next i
            Form3.Text1.text = Mid(Form3.Text1.text, 1, Len(Form3.Text1.text) - 2) & ")"
        End If
        
        If Combo2.text <> "" Then
            Form3.Text1.text = Form3.Text1.text & " As " & Combo2.text
        End If
        
        Form3.Text1.text = Form3.Text1.text & vbCrLf & vbCrLf & vbCrLf
        
        Form3.Text1.text = Form3.Text1.text & "End " & addtype & vbCrLf & vbCrLf
        
        Form3.Text1.text = Form3.Text1.text & strbefore & vbCrLf & vbCrLf
        
    End Select
    
    Call Form3.Command6_Click
    
    Unload Me
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command2_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    Call Form1.lan_changE(Form1.CH)
    Me.List1.Clear
    cscounter = 0
    Me.Combo1.Clear
    Me.Combo1.AddItem ("Integer")
    Me.Combo1.AddItem ("Single")
    Me.Combo1.AddItem ("Double")
    Me.Combo1.AddItem ("Byte")
    Me.Combo1.AddItem ("String")
    Me.Combo1.AddItem ("Boolean")
    Me.Combo1.AddItem ("Variant")
    
    Me.Combo2.Clear
    Me.Combo2.AddItem ("Integer")
    Me.Combo2.AddItem ("Single")
    Me.Combo2.AddItem ("Double")
    Me.Combo2.AddItem ("Byte")
    Me.Combo2.AddItem ("String")
    Me.Combo2.AddItem ("Boolean")
    Me.Combo2.AddItem ("Variant")
    

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub
