VERSION 5.00
Begin VB.Form DebugConsole 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Debug Message"
   ClientHeight    =   4905
   ClientLeft      =   5430
   ClientTop       =   7110
   ClientWidth     =   11145
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   4905
   ScaleWidth      =   11145
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox Debugtext 
      BackColor       =   &H00FFFFFF&
      ForeColor       =   &H00000000&
      Height          =   4335
      Left            =   0
      MultiLine       =   -1  'True
      ScrollBars      =   2  'Vertical
      TabIndex        =   1
      Top             =   0
      Width           =   11175
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Clear"
      Height          =   375
      Left            =   3360
      TabIndex        =   0
      Top             =   4440
      Width           =   4455
   End
End
Attribute VB_Name = "DebugConsole"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub Command1_Click()
    On Error GoTo ErrHand

    Me.Debugtext.text = ""

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Debugtext_Change()
    On Error GoTo ErrHand

    Open App.Path & "\debug.log" For Output As #91
    Print #91, Debugtext.text
    Close #91

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Debugtext_Change", vbExclamation
End Sub
