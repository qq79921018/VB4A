VERSION 5.00
Begin VB.Form setGuide 
   BorderStyle     =   1  'Fixed Single
   Caption         =   "设置向导"
   ClientHeight    =   5385
   ClientLeft      =   5895
   ClientTop       =   3600
   ClientWidth     =   6615
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   5385
   ScaleWidth      =   6615
   Begin VB.Frame Frame1 
      Caption         =   "你可能还需要..."
      Height          =   1815
      Left            =   120
      TabIndex        =   8
      Top             =   3480
      Width           =   6375
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   1215
         Index           =   3
         Left            =   4920
         TabIndex        =   12
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   1215
         Index           =   2
         Left            =   3360
         TabIndex        =   11
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   1215
         Index           =   1
         Left            =   1800
         TabIndex        =   10
         Top             =   360
         Width           =   1215
      End
      Begin VB.CommandButton Command2 
         Caption         =   "Command2"
         Height          =   1215
         Index           =   0
         Left            =   240
         TabIndex        =   9
         Top             =   360
         Width           =   1215
      End
   End
   Begin VB.TextBox Text1 
      Appearance      =   0  'Flat
      Height          =   735
      Left            =   120
      TabIndex        =   7
      Text            =   "Text1"
      Top             =   2640
      Width           =   4815
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Enabled         =   0   'False
      Height          =   735
      Index           =   3
      Left            =   5040
      TabIndex        =   6
      Top             =   2640
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Enabled         =   0   'False
      Height          =   735
      Index           =   2
      Left            =   5040
      TabIndex        =   5
      Top             =   1800
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Enabled         =   0   'False
      Height          =   735
      Index           =   1
      Left            =   5040
      TabIndex        =   3
      Top             =   960
      Width           =   1455
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Enabled         =   0   'False
      Height          =   735
      Index           =   0
      Left            =   5040
      TabIndex        =   1
      Top             =   120
      Width           =   1455
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      ForeColor       =   &H80000008&
      Height          =   735
      Index           =   2
      Left            =   120
      TabIndex        =   4
      Top             =   1800
      Width           =   4815
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      ForeColor       =   &H80000008&
      Height          =   735
      Index           =   1
      Left            =   120
      TabIndex        =   2
      Top             =   960
      Width           =   4815
   End
   Begin VB.Label Label1 
      Appearance      =   0  'Flat
      BackColor       =   &H80000005&
      BorderStyle     =   1  'Fixed Single
      Caption         =   "Label1"
      ForeColor       =   &H80000008&
      Height          =   735
      Index           =   0
      Left            =   120
      TabIndex        =   0
      Top             =   120
      Width           =   4815
   End
End
Attribute VB_Name = "setGuide"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub Command1_Click(Index As Integer)
Select Case Index
Case 0

    If MsgBox(Form1.CHorEN(Form1.CH, "VB4A编译器需要JDK的支持，是否下载并安装JDK？|VB4A need JDK to compile apk, do you want to download and install JDK?"), vbYesNo, "") = vbYes Then
        Form1.DownloadJDK_Click
        MsgBox Form1.CHorEN(Form1.CH, "请下载并安装，然后点击本对话框按钮继续。|Please download JDK and install it before you click OK."), vbOKOnly, ""
    End If
    
    Form1.Helpenv

Case 1

    MsgBox Form1.CHorEN(Form1.CH, "请将VB4A的文件夹剪切到某个非中文的路径下，例如【D:\programs\vb4a】，当前路径为：[|Please move vb4a to a non-unicode-named dir, such as [D:\programs\vb4a], which is currently installed in:[") & App.Path & "]", vbOKOnly, ""

Case 2

    MsgBox Form1.CHorEN(Form1.CH, "请将VB4A的文件夹剪切到某个不包括空格的路径下，例如【D:\programs\vb4a】，当前路径为：[|Please move vb4a to a dir without space, such as [D:\programs\vb4a], which is currently installed in:[") & App.Path & "]", vbOKOnly, ""

Case 3

    MsgBox Form1.CHorEN(Form1.CH, "通常这个问题是由于安装文件不完整造成，请重新下载VB4A。|This problem was caused by a broken install file, please redownload vb4a from our website."), vbOKOnly, ""

End Select
End Sub

Private Sub Command2_Click(Index As Integer)
Select Case Index
Case 0
    Form1.downloademu_Click
    MsgBox Form1.CHorEN(Form1.CH, "点击本对话框按钮后请选择VB4AEMU.zip文件进行安装，若在下载请等待下载完成。|Please download VB4AEMU before you click OK."), vbOKOnly, ""
    Form1.installemu_Click
Case 1
    Form1.postba_Click
Case 2
    Form1.regocx_Click
Case 3
    Form1.donate_Click
End Select
End Sub

Private Sub Form_Load()
Me.Frame1.Caption = Form1.CHorEN(Form1.CH, "您可能还需要...|You may also want to...")
Me.Command2(0).Caption = Form1.CHorEN(Form1.CH, "安装模拟器|Download Emulator")
Me.Command2(1).Caption = Form1.CHorEN(Form1.CH, "访问贴吧|Visit BaiduPost(NA for English)")
Me.Command2(2).Caption = Form1.CHorEN(Form1.CH, "注册组件|Regist OCX files")
Me.Command2(3).Caption = Form1.CHorEN(Form1.CH, "捐赠|Make a donation")
End Sub

Private Sub Form_Unload(Cancel As Integer)
Form1.Show
End Sub

