VERSION 5.00
Begin VB.Form FormChooser 
   BorderStyle     =   4  'Fixed ToolWindow
   Caption         =   "Form8"
   ClientHeight    =   630
   ClientLeft      =   45
   ClientTop       =   315
   ClientWidth     =   4980
   LinkTopic       =   "Form8"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   630
   ScaleWidth      =   4980
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  '����ȱʡ
   Begin VB.ComboBox Combo1 
      Height          =   300
      Left            =   120
      TabIndex        =   1
      Text            =   "Combo1"
      Top             =   120
      Width           =   3375
   End
   Begin VB.CommandButton Command1 
      Caption         =   "Command1"
      Default         =   -1  'True
      Height          =   375
      Left            =   3600
      TabIndex        =   0
      Top             =   120
      Width           =   1215
   End
End
Attribute VB_Name = "FormChooser"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public ChooseReady As Boolean
Public ChooseStr As String
Public finpos As Long

Private Sub Command1_Click()
    On Error GoTo ErrHand

    ChooseReady = True
    ChooseStr = "New " & Me.Combo1.text
    Form3.Str2add = ChooseStr
    
    Call Form3.refreshText2(finpos)
    Me.Hide

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Command1_Click", vbExclamation
End Sub

Private Sub Form_Load()
    On Error GoTo ErrHand

    ChooseStr = ""

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Load", vbExclamation
End Sub

Private Sub Form_Unload(Cancel As Integer)
    On Error GoTo ErrHand

    ChooseReady = True
    ChooseStr = ""

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Form_Unload", vbExclamation
End Sub
