Private Sub Text1_Change()
'    Set oSyntax = New CSyntax
'    oSyntax.HighLightRichEdit Text1
'    Set oSyntax = Nothing
'    Text1.SelStart = pos
    oldText = Combo1.text
End Sub

Private Sub Text1_KeyDown(KeyCode As Integer, Shift As Integer)
    On Error GoTo ErrHand'

    If Check3.Value = 1 Then
        If KeyCode = vbKeyTab Then
            Text1.SelText = Chr(vbKeyTab)
            Text1.SetFocus
            KeyCode = 0
        End If
    End If

    Exit Sub
ErrHand:
    MsgBox "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Text1_KeyDown", vbExclamation
End Sub

Private Sub Text1_Keyup(KeyCode As Integer, Shift As Integer)
    On Error GoTo ErrHand


    Dim pos As Long
    Dim StrEXT As String
    Dim StrTXT As String
    Dim POSTXT As Long
    Dim ifixStr As String
    If Check1.Value = 0 Then Exit Sub
     inComp = False

Debug.Print KeyCode

    If KeyCode = 13 Then
        Call Command10_Click
    End If

    If KeyCode = 190 Or KeyCode = 110 Then
        
        If Mid(Text1.text, Text1.SelStart, 1) = "." Then

            Call AutoComplete

            inComp = True

        End If

    End If

    If KeyCode = 13 Then
        Call Command6_Click
    End If

    tmpstrAC = Chr(KeyCode)
    'Debug.Print KeyCode, tmpstrAC

    If tmpstrAC = Chr(34) Or _
       tmpstrAC = " " Or _
       KeyCode = 187 Or _
       tmpstrAC = vbTab Or _
       tmpstrAC = "," Or _
       tmpstrAC = ":" Or _
       tmpstrAC = "(" Or _
       tmpstrAC = "+" Or _
       tmpstrAC = "-" Or _
       tmpstrAC = "*" Or _
       tmpstrAC = vbLf Or _
       tmpstrAC = "/" Or _
       temstrac = "." Or _
       tmpstrAC = Chr(13) _
       Then

        If KeyCode = 187 Then ifixStr = "=" Else ifixStr = Chr(KeyCode)

        StrEXT = ExtString
        StrTXT = Text1.text
        POSTXT = Text1.SelStart

        Debug.Print "-->"; StrEXT

        '以下代码造成文本跳动
        '           If Len(LCase(StrEXT)) >= 5 Then
        '            If Mid(LCase(StrEXT), 1, 5) = "endif" Then
        '            Debug.Print "ENDIF SEPC"
        '                If KeyCode = 13 And Mid(Text1.text, Text1.SelStart - 1, 2) = vbNewLine Then
        '                 StrEXT = Replace(StrEXT, vbCr, "")
        '                 Debug.Print "Specially "; "["; StrEXT; "]", Len(StrEXT), inComp
        '                 Mid(StrTXT, Text1.SelStart - Len(StrEXT) - 1, Len(StrEXT) + 1) = Form5.Change(StrEXT)
        '                 Debug.Print "ENDIF VBCRLF"
        '                Else
        '                 Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT) + 1) = Form5.Change(StrEXT) & ifixStr
        '                 Debug.Print "ENDIF SPC", "["; ifixStr; "]"
        '                End If
        '            End If
        '           Else
        '                If KeyCode = 13 And Mid(Text1.text, Text1.SelStart - 1, 2) = vbNewLine Then
        '                 StrEXT = Replace(StrEXT, vbCr, "")
        '                 Debug.Print "Specially "; "["; StrEXT; "]", Len(StrEXT), inComp
        '                 Mid(StrTXT, Text1.SelStart - Len(StrEXT) - 1, Len(StrEXT)) = Form5.Change(StrEXT)
        '                Else
        '                 Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT)) = Form5.Change(StrEXT)
        '                End If
        '           End If
        '           Text1.text = StrTXT
        '以上代码造成文本跳动
        
        If Check4.Value = 1 Then

            If Len(LCase(StrEXT)) >= 5 Then
                If Mid(LCase(StrEXT), 1, 5) = "endif" Then
    
                    Debug.Print "ENDIF SEPC"
                    If KeyCode = 13 And Mid(Text1.text, Text1.SelStart - 1, 2) = vbNewLine Then
                        StrEXT = Replace(StrEXT, vbCr, "")
                        Debug.Print "Specially "; "["; StrEXT; "]", Len(StrEXT), inComp
    
                        'Mid(StrTXT, Text1.SelStart - Len(StrEXT) - 1, Len(StrEXT) + 1) = Form5.Change(StrEXT)
    
                        Text1.SelStart = POSTXT - Len(StrEXT) - 2
                        Text1.SelLength = Len(StrEXT) + 1
                        Text1.SelText = Form5.Change(StrEXT)
    
                        Debug.Print "ENDIF VBCRLF"
                        Text1.SelStart = POSTXT + 1
                    Else
    
                        'Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT) + 1) = Form5.Change(StrEXT) & ifixStr
    
                        Text1.SelStart = POSTXT - Len(StrEXT) - 1
                        Text1.SelLength = Len(StrEXT) + 1
                        Text1.SelText = Form5.Change(StrEXT) & ifixStr
    
                        Debug.Print "ENDIF SPC", "["; ifixStr; "]"
                        Text1.SelStart = POSTXT + 1
                    End If
                End If
    
            Else
    
                If KeyCode = 13 And Mid(Text1.text, Text1.SelStart - 1, 2) = vbNewLine Then
                    StrEXT = Replace(StrEXT, vbCr, "")
                    Debug.Print "Specially CRLF:"; "["; StrEXT; "]", Len(StrEXT), inComp
    
                    'Mid(StrTXT, Text1.SelStart - Len(StrEXT) - 1, Len(StrEXT)) = Form5.Change(StrEXT)
    
                    Text1.SelStart = POSTXT - Len(StrEXT) - 2
                    Text1.SelLength = Len(StrEXT)
                    Text1.SelText = Form5.Change(StrEXT)
    
                    Text1.SelStart = POSTXT
                Else
    
                    'Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT)) = Form5.Change(StrEXT)
                    Debug.Print "NARMALFIX:"; "["; StrEXT; "]", Len(StrEXT)
                    Text1.SelStart = POSTXT - Len(StrEXT) - 1
                    Text1.SelLength = Len(StrEXT)
                    Text1.SelText = Form5.Change(StrEXT)
    
                    Text1.SelStart = POSTXT
                End If
    
            End If

        End If


        Text1.SelStart = POSTXT

        '==================
        'Text1.SelColor = vbBlack
        'If StrEXT <> Form5.Change(StrEXT) And Len(StrEXT) <> 0 Then
        'Debug.Print StrEXT; "<>"; Form5.Change(StrEXT)
        ' Text1.SelStart = POSTXT - Len(StrEXT) - 1
        ' Text1.SelLength = Len(StrEXT)
        ' Text1.SelColor = vbRed
        ' Debug.Print "sel:"; Text1.SelText
        ' Text1.SelStart = POSTXT
        'End If
        'Text1.SelColor = vbBlack
        '==================
        'Debug.Print "LastString：["; StrEXT; "] ChangedString: "; Form5.Change(StrEXT); " MID:["; Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT)); "]", Len(StrEXT), Len(Mid(StrTXT, Text1.SelStart - Len(StrEXT), Len(StrEXT)))
    End If




    Exit Sub
ErrHand:
    Debug.Print "Error Number: " & vbTab & Err.Number & vbCrLf & "Error Description: " & vbTab & Err.Description & vbCrLf & "Error Sub: " & vbTab & "Text1_Keyup", vbExclamation
End Sub
